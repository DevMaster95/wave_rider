﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GestureController : MonoBehaviour
{
    [SerializeField] private float stationaryMovementThreshold = 0.2f;
    [SerializeField] private float stationaryTimeThreshold = 0.1f;
    [SerializeField] private float minGestureMovementThreshold = 5f;
    private float mouseDownTime = 0;

    Vector2 firstPressPos;
    Vector2 secondPressPos;
    Vector2 currentSwipe;

    public delegate void SwipeUp();
    public delegate void SwipeDown();
    public delegate void SwipeLeft();
    public delegate void SwipeRight();

    public static event SwipeUp SwipeUp_Event;
    public static event SwipeDown  SwipeDown_Event;
    public static event SwipeLeft  SwipeLeft_Event;
    public static event SwipeRight SwipeRight_Event;


    private bool isHolding = false;
    public bool IsHolding 
    {
        get
        {
            return isHolding;
        }
        private set
        {
            isHolding = value;
        }
    }

    // Update is SwipeRight();called once per frame
    void Update()
    {
        if (Time.timeScale == 0)
        {
            CancleGestures();
            return;
        }

#if UNITY_STANDALONE
        SwipeMouse();
        return;
        #endif

        SwipeTouch();
    }

    public void SwipeTouch()
    {
        if (Input.touches.Length == 0) return;
        
        Touch t = Input.GetTouch(0);
        if (t.phase == TouchPhase.Began)
        {
            //save began touch 2d point
            firstPressPos = new Vector2(t.position.x, t.position.y);
        }
        if (t.phase == TouchPhase.Stationary)
        {
            IsHolding = true;
        }
        if (t.phase == TouchPhase.Ended)
        {
            if(IsHolding)
            {
                IsHolding = false;
                return;
            }

            IsHolding = false;

            //save ended touch 2d point
            secondPressPos = new Vector2(t.position.x, t.position.y);

            //create vector from the two points
            currentSwipe = new Vector3(secondPressPos.x - firstPressPos.x, secondPressPos.y - firstPressPos.y);

            //normalize the 2d vector
            currentSwipe.Normalize();

            //swipe upwards
            if (currentSwipe.y > 0 && currentSwipe.x > -minGestureMovementThreshold && currentSwipe.x < minGestureMovementThreshold)
            {
                Debug.Log("up swipe");
                SwipeUp_Event?.Invoke();
            }
            //swipe down
            if (currentSwipe.y < 0 && currentSwipe.x > -minGestureMovementThreshold && currentSwipe.x < minGestureMovementThreshold)
            {
                Debug.Log("down swipe");
                SwipeDown_Event?.Invoke();
            }
            //swipe left
            if (currentSwipe.x < 0 && currentSwipe.y > -minGestureMovementThreshold && currentSwipe.y < minGestureMovementThreshold)
            {
                Debug.Log("left swipe");
                SwipeLeft_Event?.Invoke();

            }
            //swipe right
            if (currentSwipe.x > 0 && currentSwipe.y > -minGestureMovementThreshold && currentSwipe.y < minGestureMovementThreshold)
            {
                Debug.Log("right swipe");
                SwipeRight_Event?.Invoke();

            }
        }        
    }

    public void SwipeMouse()
    {
        if (Input.GetMouseButtonDown(0))
        {
            //save began touch 2d point
            firstPressPos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            mouseDownTime = Time.time;
        }

        if(Input.GetMouseButton(0))
        {
            //if mouse is held down
            Vector2 currentMousePosition = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            Vector2 totalMovement = new Vector2(currentMousePosition.x - firstPressPos.x, currentMousePosition.y - firstPressPos.y);

            float timeHeld = Time.time - mouseDownTime;
            if (totalMovement.magnitude < stationaryMovementThreshold && timeHeld > stationaryTimeThreshold)
            {
                //Mouse is still down and hasn't moved much
                IsHolding = true;
            }
        }

        if (Input.GetMouseButtonUp(0))
        {
            if (IsHolding)
            {
                IsHolding = false;
                return;
            }

            //save ended touch 2d point
            secondPressPos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);

            //create vector from the two points
            currentSwipe = new Vector2(secondPressPos.x - firstPressPos.x, secondPressPos.y - firstPressPos.y);

            //normalize the 2d vector
            currentSwipe.Normalize();

            //swipe upwards
            if (currentSwipe.y > minGestureMovementThreshold)// && currentSwipe.x > -minGestureMovementThreshold && currentSwipe.x < minGestureMovementThreshold)
            {
                Debug.Log("up swipe");
                SwipeUp_Event?.Invoke();
            }
            //swipe down
            else if (currentSwipe.y < -minGestureMovementThreshold)// && currentSwipe.x > -minGestureMovementThreshold && currentSwipe.x < minGestureMovementThreshold)
            {
                Debug.Log("down swipe");
                SwipeDown_Event?.Invoke();
            }
            //swipe left
            else if (currentSwipe.x < -minGestureMovementThreshold)// && currentSwipe.y > -minGestureMovementThreshold && currentSwipe.y < minGestureMovementThreshold)
            {
                Debug.Log("left swipe");
                SwipeLeft_Event?.Invoke();

            }
            //swipe right
            else if (currentSwipe.x > minGestureMovementThreshold)// && currentSwipe.y > -minGestureMovementThreshold && currentSwipe.y < minGestureMovementThreshold)
            {
                Debug.Log("right swipe");
                SwipeRight_Event?.Invoke();
            }
        }
    }

    internal void CancleGestures()
    {
        IsHolding = false;
        mouseDownTime = 0;

        firstPressPos = Vector2.zero;
        secondPressPos = Vector2.zero;
        currentSwipe = Vector2.zero;
    }
}
