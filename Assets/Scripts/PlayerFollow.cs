﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class PlayerFollow : MonoBehaviour
{
    [SerializeField] private float playerHeight;

    [SerializeField] private Transform player;
    [SerializeField] private Transform camera;

    [SerializeField] private float playerMaxHieght;


    [SerializeField] private float maxZPos;
    [SerializeField] private float minZPos;
    private float zRange;

    [SerializeField] private float maxYPos;
    [SerializeField] private float minYPos;
    private float yRange;

    [SerializeField] private float maxXPos;
    [SerializeField] private float minXPos;
    private float xRange;

    [SerializeField] private float followDamping = 2f;

    void Update()
    {
        zRange = maxZPos - minZPos;
        yRange = maxYPos - minYPos;
        xRange = maxXPos - minXPos;

        Vector3 currentPosition = transform.position;
        Vector3 newPosition = new Vector3(player.transform.position.x, currentPosition.y, currentPosition.z);
        transform.position = newPosition;

        //Use player y as the gage for camera position
        float posPhase = player.position.y / playerMaxHieght;
        Mathf.Clamp(posPhase, 0, 2);

        float newX = (xRange * posPhase) + minXPos;
        float newY = (yRange * posPhase) + minYPos;
        float newZ = (zRange * posPhase) + minZPos;

        //Mathf.Lerp();
        camera.localPosition = Vector3.Lerp(camera.localPosition, new Vector3(newX, newY, -newZ), followDamping * Time.deltaTime);
    }
}
