﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CapsuleCollider))]
[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(GestureController))]
[RequireComponent(typeof(AudioSource))]
public class Player : MonoBehaviour
{
    #region Components
    private Rigidbody m_rigidbody;
    private Animator m_animator;
    private CapsuleCollider m_collider;
    private GestureController m_gestureController;
    private AudioSource m_audioSource;

    private Rigidbody[] ragdollBodies;
    private Collider[] ragdollColliders;

    [SerializeField] private GameObject surfboard = null;
    private Animator surfBoardAnimator = null;
    #endregion

    #region Player Controller Variables
    [Header("Movement Variables")]
    [SerializeField] private float runningSpeed = 10f;
    [SerializeField] private float slopeBoostMultiplier = 0.02f;
    [SerializeField] private float rotationalGravityBoostMultiplier = 300f;
    [SerializeField] private float gravityMultiplier = 20.5f;
    [SerializeField] private float rampPositonOffest = 0.4f;
    [SerializeField, Tooltip("The angle that causes death between the normal of the player and the colliding floor normal")] 
    private float deathAngle = 15f;
    /// <summary>
    /// The time the player leaves the ramp
    /// </summary>
    private float liftoffTime;

    [SerializeField, Tooltip("The max angle the player can reach while being forced down")]
    private float maxFallingAngleAngle = 65f;
    [SerializeField] private float maxSpeed;
    [SerializeField] private float minSpeed;
    private float maxSpeedOnCurrentRamp;
    private float currentSpeed = 10f;

    [Header("Raycast Lengths")]
    [SerializeField, Tooltip("The distance above the ground that the player follows")] private float groundHeightOffset = 2f;
    [SerializeField] private float floorCheckDistance = 0.3f;
    [SerializeField] private float rampApexCheckDistance = 10f;

    /// <summary>
    /// The last position detect on the current ramp. When passed player is flying again
    /// </summary>
    private Vector3 lastFoundRampPositon = new Vector3(999,0,0);
    /// <summary>
    /// The normal of the last found ramp position. Used for debugging
    /// </summary>
    private Vector3 lastFoundRampPositonNormal;

    [SerializeField] private LayerMask floorDetectionMask;
    #endregion

    #region State Variables
    /// <summary>
    /// Running is only for the initial first ramp at the start section
    /// </summary>
    private bool running = false;
    /// <summary>
    /// When the player is moving on the curve
    /// </summary>
    private bool surfing = false;
    /// <summary>
    /// when player has left the ramp and going airborn
    /// </summary>
    private bool flying = false;
    /// <summary>
    /// when the player velocity begins to go down (-y)
    /// </summary>
    private bool falling = false;
    /// <summary>
    /// When the play has/is collided with the floor
    /// </summary>
    private bool grounded = true;
    /// <summary>
    /// Set when the player dies
    /// </summary>
    private bool isDead = false;
    #endregion

    #region Tricking Variables
    [Header("Trick Variables")]
    private int tricksComplete = 1;
    private float trickStartedTime = 0;
    [SerializeField] private float trickDuration = 0.6f;
    #endregion

    #region Audio
    [Header("Audio")]
    [SerializeField] private AudioClip splatSFX;
    [SerializeField] private AudioClip splashSFX;
    [SerializeField] private AudioSource gameMusic;
    #endregion

    #region Unity Functions
    private void Start()
    {
        m_rigidbody = gameObject.GetComponent<Rigidbody>();
        m_animator = gameObject.GetComponent<Animator>();
        m_collider = gameObject.GetComponent<CapsuleCollider>();
        m_gestureController = GetComponent<GestureController>();
        m_audioSource = GetComponent<AudioSource>();

        surfBoardAnimator = surfboard.GetComponent<Animator>();

        ragdollBodies = GetComponentsInChildren<Rigidbody>();
        ragdollColliders = GetComponentsInChildren<Collider>();

        m_rigidbody.useGravity = false;
        m_collider.enabled = false;

        ToggleRagdoll(false);

        StartCoroutine(DelayStart());
    }

    private void OnDisable()
    {
        GestureController.SwipeDown_Event -= SwipeDown;
        GestureController.SwipeUp_Event -= SwipeUp;
        GestureController.SwipeLeft_Event -= SwipeLeft;
        GestureController.SwipeRight_Event -= SwipeRight;
    }
      
    private void Update()
    {
        if (transform.position.y < -2.5)
        {
            PlaySFX(false);
            Die();
        }

        if (isDead) return;

        if(running)
        {
            m_rigidbody.velocity = transform.forward * runningSpeed;
        }
        else if(surfing)
        {
            HandleSurfing();
        }
        else if(flying || falling)
        {
            HandleFlyingInputs();
        }

        CheckRampPosition();
    }

    private void LateUpdate()
    {
        if (isDead) return;

        if (grounded)
        {
            //Check for ground and use normal to rotate player
            RaycastHit floorCheckHit;
            Vector3 floorRaycastOrigin = transform.position + (transform.up * 1.5f);
            if (Physics.Raycast(floorRaycastOrigin, transform.TransformDirection(Vector3.down), out floorCheckHit, floorCheckDistance, floorDetectionMask))
            {
                //Set player up tp ground normal
                transform.up = floorCheckHit.normal;
                //offest the player by 90 degrees to face forward
                transform.RotateAround(transform.position, transform.up, 90);

                //Raise the player of the ground slightly so they glide along better
                Vector3 newPositionWithYOffset = transform.position;
                newPositionWithYOffset.y = (floorCheckHit.point + (floorCheckHit.normal * groundHeightOffset)).y;
                transform.position = newPositionWithYOffset;

                Debug.DrawRay(floorRaycastOrigin, transform.TransformDirection(Vector3.down) * floorCheckDistance, Color.green);
                Debug.DrawRay(floorCheckHit.point, floorCheckHit.normal * floorCheckDistance, Color.blue);
            }
            else
            {
                Debug.DrawRay(floorRaycastOrigin, transform.TransformDirection(Vector3.down) * floorCheckDistance, Color.red);
            }

            RaycastHit rampCheckHit;
            //Look forward for the tip of ramp
            if (Physics.Raycast(transform.position + new Vector3(0, 1, 0), transform.TransformDirection(Vector3.forward), out rampCheckHit, rampApexCheckDistance, floorDetectionMask))
            {
                lastFoundRampPositon = rampCheckHit.point;
                lastFoundRampPositonNormal = rampCheckHit.normal;

                Debug.DrawRay(transform.position + new Vector3(0, 1, 0), transform.TransformDirection(Vector3.forward) * rampApexCheckDistance, Color.green);
                Debug.DrawRay(rampCheckHit.point, rampCheckHit.normal * rampApexCheckDistance, Color.blue);
            }
            else
            {
                Debug.DrawRay(transform.position + new Vector3(0, 1, 0), transform.TransformDirection(Vector3.forward) * rampApexCheckDistance, Color.red);
            }

            Debug.DrawRay(lastFoundRampPositon, lastFoundRampPositonNormal * floorCheckDistance, Color.blue);
        }

        HandleFlying();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (!collision.gameObject.CompareTag("Floor")) return;

        //Don't want to deal with this is we are not landing for the first time from a flight
        if (!falling || grounded) return;

        if (Time.time < trickStartedTime + trickDuration)
        {
            Die();
            PlaySFX(true);
            return;
        }


        tricksComplete = 1;

        //Handle Landing
        grounded = true;
        flying = false;
        falling = false;
        surfing = true;

        m_gestureController.CancleGestures();

        //calculate limit so player only increases speed by 10% per wave
        maxSpeedOnCurrentRamp = currentSpeed * 1.5f;
        Mathf.Clamp(maxSpeedOnCurrentRamp, minSpeed, maxSpeed);

        //Check for death
        Vector3 targetDir = transform.up;
        float angle = Vector3.Angle(targetDir, collision.GetContact(0).normal);
        Debug.Log($"Angle of collition is {angle}");

        //No More Tricks while grounded
        GestureController.SwipeDown_Event -= SwipeDown;
        GestureController.SwipeUp_Event -= SwipeUp;
        GestureController.SwipeLeft_Event -= SwipeLeft;
        GestureController.SwipeRight_Event -= SwipeRight;

        if (angle > deathAngle)
        {
            PlaySFX(true);
            Die();
        }
        else
        {
            GameCanvas.Instance.Landed();
        }
    }
    #endregion

    private void StartGame()
    {
        running = true;
        m_animator.SetBool("running", true);
        surfBoardAnimator.SetBool("Running", true);

        flying = false;
        falling = false;

        maxSpeedOnCurrentRamp = 6f;
    }

    private void CheckRampPosition()
    {
        //Check to see if player has passed the tip of the last ramp they were on. Then start flying!
        if (transform.position.x >= (lastFoundRampPositon.x - rampPositonOffest) && lastFoundRampPositon != Vector3.zero)
        {
            //Check if its the first time the player is leaving a ramp
            if (running)
            {
                //Switch to surfing animations
                m_animator.SetBool("surfing", true);
                surfBoardAnimator.SetBool("Surfing", true);
            }

            //Set this back to unset
            lastFoundRampPositon = Vector3.zero;

            m_rigidbody.useGravity = true;

            liftoffTime = Time.time;
            m_gestureController.CancleGestures();

            grounded = false;
            surfing = false;
            running = false;
            falling = false;
            flying = true;

            //Listen to see if trick inputs were made
            GestureController.SwipeDown_Event += SwipeDown;
            GestureController.SwipeUp_Event += SwipeUp;
            GestureController.SwipeLeft_Event += SwipeLeft;
            GestureController.SwipeRight_Event += SwipeRight;
        }
    }

    private IEnumerator DelayStart()
    {
        yield return new WaitForSeconds(2f);

        StartGame();    
    }

    private IEnumerator DelayScore()
    {
        GameCanvas.Instance.PlayerDied();

        yield return new WaitForSeconds(2f);

        GameCanvas.Instance.ShowScore();

    }

    private void HandleFlying()
    {
        //Detect if the playing in now falling
        if (flying && m_rigidbody.velocity.y < 0)
        {
            flying = false;
            falling = true;
            m_collider.enabled = true;
        }

        //While flying set the character to look in the direction it's traveling (Which emulates that ark movement we are looking for)
        if (flying || falling)
        {
            transform.forward = m_rigidbody.velocity.normalized;
            currentSpeed = m_rigidbody.velocity.magnitude;
        }
    }

    private void HandleSurfing()
    {
        //If travling downwards on the slopes - accelerate
        if(m_rigidbody.velocity.y < 0)
        {
            currentSpeed = currentSpeed + (currentSpeed * slopeBoostMultiplier * Time.deltaTime);
            Mathf.Clamp(currentSpeed, minSpeed, maxSpeedOnCurrentRamp);
        }

        m_rigidbody.velocity = transform.forward * currentSpeed;

    }

    private void HandleFlyingInputs()
    {
        if(m_gestureController.IsHolding)
        {
            if(transform.rotation.eulerAngles.x < maxFallingAngleAngle)
            {
                //accelerate/ point the player downwards
                //m_rigidbody.AddForce(Vector3.down * gravityBoostMultiplier * Time.deltaTime, ForceMode.VelocityChange);
                m_rigidbody.velocity = Quaternion.AngleAxis(rotationalGravityBoostMultiplier * Time.deltaTime, transform.right) * m_rigidbody.velocity;
                //Vector3 newRotation = Quaternion.AngleAxis(gravityBoostMultiplier * Time.deltaTime, transform.right) * transform.rotation.eulerAngles;
                
            }

            transform.position += Vector3.down * gravityMultiplier * Time.deltaTime;
            Debug.Log("Holding Down");
        }
    }

    private void ToggleRagdoll(bool state)
    {
        foreach (Rigidbody rb in ragdollBodies)
        {
            rb.isKinematic = !state;
        }

        foreach (Collider coll in ragdollColliders)
        {
            coll.enabled = state;
        }

        m_animator.enabled = !state;
        surfBoardAnimator.enabled = !state;
        m_rigidbody.isKinematic = state;
        m_collider.enabled = !state;

    }

    private void Die()
    {
        gameMusic.Stop();

        transform.position += new Vector3(0, 0.2f, 0);
        isDead = true;

        ToggleRagdoll(true);

        //surfboard.GetComponent<Rigidbody>().AddForce(new Vector3(0, 10, 0), ForceMode.Impulse);
        surfboard.GetComponent<Collider>().enabled = false;

        StartCoroutine(DelayScore());
    }

    private void PlaySFX(bool isSplatSFX)
    {
        if(isSplatSFX)
        {
            m_audioSource.clip = splatSFX;
        }
        else
        {
            m_audioSource.clip = splashSFX;
        }

        m_audioSource.Play();
    }

    internal void Revive()
    {
        throw new NotImplementedException();
    }

    private void SwipeUp()
    {
        TryTrick("FrontFlip", "Front Flip");
    }

    private void SwipeDown()
    {
        TryTrick("BackFlip", "Back Flip");
    }

    private void SwipeLeft()
    {
        TryTrick("LeftSpin", "360 Varial");
    }

    private void SwipeRight()
    {
        TryTrick("RightSpin", "360 Varial");
    }

    private void TryTrick(string trickName, string displayName)
    {
        if (Time.time > trickStartedTime + trickDuration)
        {
            trickStartedTime = Time.time;
            m_animator.SetTrigger(trickName);
            GameCanvas.Instance.DidTrick(displayName, tricksComplete);
            tricksComplete++;
        }
    }
}
