﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using TMPro;

public class ScoreScreen : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI score_txt;
    [SerializeField] private GameObject highscore_txt;
    [SerializeField] private Player player;

    public void SetScore(int score)
    {
        if(PlayerPrefs.HasKey("Highscore"))
        {
            int bestscore = PlayerPrefs.GetInt("Highscore");

            if(bestscore < score)
            {
                highscore_txt.SetActive(true);
                PlayerPrefs.SetInt("Highscore", score);
            }
        }
        else
        {
            PlayerPrefs.SetInt("Highscore", score);
            highscore_txt.SetActive(true);
        }

        score_txt.text = score.ToString();
    }

    public void Revive()
    {
        player.Revive();
    }

    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void Quit()
    {
        SceneManager.LoadScene("Menu_Scene");
    }
}
