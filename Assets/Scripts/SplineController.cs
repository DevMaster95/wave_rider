﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PathCreation;
using PathCreation.Examples;
public class SplineController : MonoBehaviour
{
    [SerializeField] private PathCreator spline;
    [Header("Segment Length")]
    [SerializeField] private float minSegmentLength;
    [SerializeField] private float maxSegmentLength;

    [Header("Peak Height Range")]
    [SerializeField] private float minPeakHeight = 4;
    [SerializeField] private float maxPeakHeight = 6;

    [Header("Troff Height Range")]
    [SerializeField] private float minTroffHeight = 0;
    [SerializeField] private float maxTroffHeight = 2;
    void Start()
    {
        //ClearSegments();

        float lastZ = 18f;

        bool addPeakNext = true;
        for (int i = 0; i < 50; i++)
        {
            //get the position of the last segment
            float nextZ = lastZ + Random.Range(minSegmentLength, maxSegmentLength);
            lastZ = nextZ;

            float nextY;
            if (addPeakNext)
            {
                //Roll 2 sets and divide by 2 so there is a bios towards the center of the 2 numbers like rolling 7's on a pair of dice
                float dice1 = Random.Range(minPeakHeight, maxPeakHeight);
                float dice2 = Random.Range(minPeakHeight, maxPeakHeight);
                nextY = (dice1 + dice2) / 2;
            }
            else
            {
                nextY = Random.Range(minTroffHeight, maxTroffHeight);
            }

            //Debug.Log($"Adding points @ 0:X {nextY}:Y {nextZ}:Z");
            spline.bezierPath.AddSegmentToEnd(new Vector3(0, nextY, nextZ));

            addPeakNext = !addPeakNext;
        }

        spline.UpdatePathExt();
        spline.GetComponent<RoadMeshCreator>().GenerateNewRoad();
    }

    public void ClearSegments()
    {
        for (int i = 0; i < spline.bezierPath.GetPoints.Count; i++)
        {
            spline.bezierPath.DeleteSegment(i);
        }
    }
}
