﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

[RequireComponent(typeof(AudioSource))]
public class GameCanvas : MonoBehaviour
{
    public static GameCanvas Instance;
    public static AudioSource audioSource;

    [SerializeField] private ScoreScreen scoreScreen;
    [SerializeField] private TextMeshProUGUI score_txt;
    [SerializeField] private TextMeshProUGUI trick_txt;
    [SerializeField] private TextMeshProUGUI trickScore_txt;
    [SerializeField] private TextMeshProUGUI multiplier_txt;

    [SerializeField] private int pointsPerUnitsTraveled = 10;
    private int currentTrickScore = 0;
    private int totalScoreFromTricks = 0;
    private int score = 0;
    private int Score
    {
        get
        {
            return score;
        }
        set
        {
            score = value;
            score_txt.text = score.ToString();
        }
    }

    private Player player;
    private float playerStartPos;
    private float distanceTraveled;

    public void Start()
    {
        Instance = this;
        player = FindObjectOfType<Player>();
        playerStartPos = player.transform.position.x;

        audioSource = GetComponent<AudioSource>();

        trick_txt.gameObject.SetActive(false);
        trickScore_txt.gameObject.SetActive(false);
        multiplier_txt.gameObject.SetActive(false);
    }

    public void ShowScore()
    {
        //hide the previous score
        score_txt.gameObject.SetActive(false);

        //show the score screen
        scoreScreen.SetScore(score);
        scoreScreen.gameObject.SetActive(true);

        trick_txt.gameObject.SetActive(false);
        trickScore_txt.gameObject.SetActive(false);
        multiplier_txt.gameObject.SetActive(false);
    }
    public void PlayerDied()
    {
        trick_txt.color = Color.red;
        trickScore_txt.color = Color.red;
        multiplier_txt.color = Color.red;
    }

    public void Update()
    {
        //minus the player start position in case the play isn't actually placed on 0
        distanceTraveled = player.transform.position.x - playerStartPos;

        float distanceScore = distanceTraveled * (float)pointsPerUnitsTraveled;

        Score = totalScoreFromTricks + (int)distanceScore;
    }

    public void DidTrick(string trickName, int multiplier)
    {
        currentTrickScore += 100 * multiplier;

        trick_txt.gameObject.SetActive(true);
        trickScore_txt.gameObject.SetActive(true);
        multiplier_txt.gameObject.SetActive(true);

        trick_txt.text = trickName;
        trickScore_txt.text = $"+{currentTrickScore}";
        multiplier_txt.text = $"X{multiplier}";
    }

    public void Landed()
    {
        totalScoreFromTricks += currentTrickScore;

        if(currentTrickScore > 0)
            audioSource.Play();

        currentTrickScore = 0;

        trick_txt.gameObject.SetActive(false);
        trickScore_txt.gameObject.SetActive(false);
        multiplier_txt.gameObject.SetActive(false);
    }
}
